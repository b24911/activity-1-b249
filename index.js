//alert("Hello, B249!");


// Global Object
// Arrays
let students = ["John", "Joe", "Jane", "Jessie"];
console.log(students);

//What array method can we use to add an item at the end of the array?
students.push("Raf");
console.log(students);

//What array method can we use to add an item at the start of the array?
students.unshift("Pinky");
console.log(students);

// What array method does the opposite of push()?
students.pop();
console.log(students);

// What array method does the opposite of unshift()?
students.shift()
console.log(students);


//What is the difference between splice() and slice method()?
/*
	splice - changes the original array
		   - mutator methods
		   - remove and adding items from a starting index

	slice - Non-Mutator methods
		  - copies a portion from starting index and returns a new array from it

	
	Iterator Methods
		- loop over the items of an array

	forEach()
	map()
	every()

*/

//Mini Activity:

let arrNum = [15, 20, 25, 30, 11, 7];

/*
	Using forEach, check if every item in the array is divisible by 5. if they are, log in the console "<num> is divisible by 5" if not, log false.

	Kindly send the screenshot of your output in our google chat.
	
	6:50PM

*/

arrNum.forEach(num => {
	if(num % 5 === 0) {
		console.log(`${num} is divisible by 5`);
	} else {
		console.log(false);
	}
});


//Math Object - allows you to perform mathematical tasks on numbers
//Mathematical constants
console.log(Math);
console.log(Math.E); //Euler's number
console.log(Math.PI); //PI
console.log(Math.SQRT2); //square root of 2
console.log(Math.SQRT1_2); //square root of 1/2
console.log(Math.LN2); //natural logarithm of 2
console.log(Math.LN10); //natural logarithm of 10
console.log(Math.LOG2E); //base 2 of logarithm of E
console.log(Math.LOG10E); //base 10 of logarithm of E


//Methods for rounding a number to an integer
console.log(Math.round(Math.PI)); //3
console.log(Math.ceil(Math.PI)); //4
console.log(Math.floor(Math.PI)); //3
console.log(Math.trunc(Math.PI)); //3

//returns a square root of a number
console.log(Math.sqrt(3.14)); //1.772004514666935

//lowest value in a list of arguments
console.log(Math.min(-1, -2, -4, -100, 0, 1, 3, 5, 6)); //-100

//highest value in a list of arguments
console.log(Math.max(-1, -2, -4, -100, 0, 1, 3, 5, 6)); //6

//Exponent Operator
const number = 5 ** 2;
console.log(number);

console.log(Math.pow(5, 2));



/*
========
ACTIVITY
========
I. QUIZ

1 How do you create arrays in JS?
	via the use of array literals - []

2 How do you access the first character of an array?
	arrayName[0]

3 How do you access the last character of an array?
	arrayName[arrayName.length-1]

4 What array method searches for, and returns the index of a given value in an array? 
This method returns -1 if given value is not found in the array.
	indexOf()

5 What array method loops over all elements of an array, performing a user-defined function on each iteration?
	forEach()

6.   What array method creates a new array with elements obtained from a user-defined function?
	map()

7.   What array method checks if all its elements satisfy a given condition?
	every()

8.   What array method checks if at least one of its elements satisfies a given condition?
	some()

9.   True or False: array.splice() modifies a copy of the array, leaving the original unchanged.
	False - splice() changes the original array

10.   True or False: array.slice() copies elements from original array and returns these as a new array.
	True - slice() does NOT modify the original array

*/

// II. FUNCTION CODING
// 1. Create a function named addToEnd that will add a passed in string to the end of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Ryan" as arguments when testing.
const addToEnd = (arr, element) => {
    if(typeof element !== "string"){
        return "error - can only add strings to an array";
    }
    arr.push(element);
    return arr;
}
console.log("Question 1:")
//test input
console.log(addToEnd(students, "Ryan")); //["John", "Joe", "Jane", "Jessie", "Ryan"]
//validation check
console.log(addToEnd(students, 45)); //"error - can only add strings to an array"


//2. Create a function named addToStart that will add a passed in string to the start of a passed in array. If element to be added is not a string, return the string "error - can only add strings to an array". Otherwise, return the updated array. Use the students array and the string "Tess" as arguments when testing.
const addToStart = (arr, element) => {
    if(typeof element !== "string"){
        return "error - can only add strings to an array";
    }
    arr.unshift(element);
    return arr;
}
console.log("Question 2:")
//test input
console.log(addToStart(students, "Tess")); //["Tess", "John", "Joe", "Jane", "Jessie", "Ryan"]
//validation check
console.log(addToStart(students, 33)); //"error - can only add strings to an array"

// 3. Create a function named elementChecker that will check a passed in array if at least one of its elements has the same value as a passed in argument. If array is empty, return the message "error - passed in array is empty". Otherwise, return a boolean value depending on the result of the check. Use the students array and the string "Jane" as arguments when testing.
const elementChecker = (arr, elementToBeChecked) => {
    if(arr.length === 0){
        return "error - passed in array is empty";
    }
    return arr.some(element => element === elementToBeChecked);    
}
console.log("Question 3:")
//test input
console.log(elementChecker(students, "Jane")); //true
//validation check
console.log(elementChecker([], "Jane")); //"error - passed in array is empty"


/*
	4. Create a function named checkAllStringsEnding that will accept a passed in array and a character. The function will do the ff:

		if array is empty, return "error - array must NOT be empty"
		if at least one array element is NOT a string, return "error - all array elements must be strings"
		if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		if every element in the array ends in the passed in character, return true. Otherwise return false.

Use the students array and the character "e" as arguments when testing.
*/
const checkAllStringsEnding = (arr, char) => {
    if(arr.length === 0) return "error - array must NOT be empty";

    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";

    if(typeof char !== "string") return "error - 2nd argument must be of data type string";

    if(char.length > 1) return "error - 2nd argument must be a single character";

    return arr.every(element => element[element.length-1] === char);
}
console.log("Question 4:")
//test input
console.log(checkAllStringsEnding(students, "e")); //false
//validation checks
console.log(checkAllStringsEnding([], "e")); //"error - array must NOT be empty"
console.log(checkAllStringsEnding(["Jane", 2], "e")); //"error - all array elements must be strings"
console.log(checkAllStringsEnding(students, 4)); //"error - 2nd argument must be of data type string"
console.log(checkAllStringsEnding(students, "el")); //"error - 2nd argument must be a single character"

//5. Create a function named stringLengthSorter that will take in an array of strings as its argument and sort its elements in an ascending order based on their lengths. If at least one element is not a string, return "error - all array elements must be strings". Otherwise, return the sorted array. Use the students array to test.
const stringLengthSorter = (arr) => {
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    arr.sort((elementA, elementB) => {
        return elementA.length - elementB.length;
    })
    return arr;
}
console.log("Question 5:")
//test input
console.log(stringLengthSorter(students)); //['Joe', 'Tess', 'John', 'Jane', 'Ryan', 'Jessie'];
//validation check
console.log(stringLengthSorter([37, "John", 39, "Jane"])); //"error - all array elements must be strings"


/*
	6. Create a function named startsWithCounter that will take in an array of strings and a single character. The function will do the ff:

		if array is empty, return "error - array must NOT be empty"
		if at least one array element is NOT a string, return "error - all array elements must be strings"
		if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
		if 2nd argument is more than 1 character long, return "error - 2nd argument must be a single character"
		return the number of elements in the array that start with the character argument, must be case-insensitive

	Use the students array and the character "J" as arguments when testing.
*/
const startsWithCounter = (arr, char) => {
    if(arr.length === 0) return "error - array must NOT be empty";
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    if(typeof char !== "string") return "error - 2nd argument must be of data type string";
    if(char.length > 1) return "error - 2nd argument must be a single character";
    let result = 0;
    arr.forEach(element => {
        if(element[0].toLowerCase() === char.toLowerCase()) result++;
    })
    return result;
}
//test input
console.log(startsWithCounter(students, "j")); //4

/*
	7. Create a function named likeFinder that will take in an array of strings and a string to be searched for. The function will do the ff:

	if array is empty, return "error - array must NOT be empty"
	if at least one array element is NOT a string, return "error - all array elements must be strings"
	if 2nd argument is NOT of data type string, return "error - 2nd argument must be of data type string"
	return a new array containing all elements of the array argument that contain the string argument in it, must be case-insensitive

	Use the students array and the string "jo" as arguments when testing.
*/
const likeFinder = (arr, str) => {
    if(arr.length === 0) return "error - array must NOT be empty";
    if(arr.some(element => typeof element !== "string")) return "error - all array elements must be strings";
    if(typeof str !== "string") return "error - 2nd argument must be of data type string";
    let result = [];
    arr.forEach(element => {
        if(element.toLowerCase().includes(str.toLowerCase())) result.push(element);
    })
    return result;
}
//test input
console.log(likeFinder(students, "jo")); //["Joe", "John"]

// 8. Create a function named randomPicker that will take in an array and output any one of its elements at random when invoked. Pass in the students array as an argument when testing.
const randomPicker = (arr) => {
    if(arr.length === 0) return "error - array must NOT be empty";
    return arr[Math.floor(Math.random() * arr.length)];
}
//test input
console.log(randomPicker(students)); //"Ryan"
console.log(randomPicker(students)); //"John"
console.log(randomPicker(students)); //"Jessie"


//Activity:

/*


1.Array literals
2.index(0) // 
3.array.length -1 // arrayName[arrayName.lenght-1]
4.findIndex // indexOf
5.forEach()
6.map() 
7.every()
8.find() // some()
9.false // splice is a mutator method it changes the original array
10.true // slice is a non mutator method


*/

function addToEnd(array, name){

		if (typeof name !== 'string'){
			return "Error can only add string to an array"
		}else {
			array.push(name);
			return array;
		}
}
// const addToEnd = (arr, element) => {
// 	if(typeof element !== "string"){
// 		return "error"
// 	}
// 	arr.push(element);
// 	return arr
// }

function addToStart(array, name){

		if (typeof name !== 'string'){
			console.log("Error can only add string to an array")
		}else {
			students.unshift(name);
			console.log(students);
		}
}
/*
const addToEnd = (arr, element) =>{
	if(typeof element !== "string"){
	return "error - can only add strings to an array"
	}
}
*/

function elementChecker(array, name){
	let foundName = students.includes(name);
	if(arr.length === 0){
		console.log(true);
	}else{
		return "error passed an empty array";
	}

/*
const elementChecker = (arr, elementToBeChecked) => {
	if(arr.length === 0){
	return "error"
	}
}return arr.some(element => element === elementToBeChecked);
*/
function checkAllStringsEnding(array, Letter){
	let filteredStudents = students.filter(function(students){
	return students.toLowerCase().includes('e');
});
	//some and every method
}


function stringLengthSorter(){

}


